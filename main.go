package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"time"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cbind"
	"gitlab.com/tslocum/cview"
)

var (
	mainTimer    *cview.TextView
	app          *cview.Application
	timerStart   time.Time
	timerValue   time.Duration
	timerRunning bool

	listeners []net.Listener
)

func handleUpdateTimer() {
	t := time.NewTicker(time.Second / 60)
	for range t.C {
		if !timerRunning {
			mainTimer.SetText(fmt.Sprintf("%s", timerValue.Round(time.Millisecond/100)))
			app.Draw()
			continue
		}

		mainTimer.SetText(fmt.Sprintf("%s", time.Since(timerStart).Round(time.Millisecond/100)))
		app.Draw()
	}
}

func nextSplit() {
	timerValue = time.Since(timerStart)
	timerRunning = !timerRunning
	if timerRunning {
		timerStart = time.Now()
	}
}

func listenControlFile(controlDir string) {
	l, err := net.Listen("unix", path.Join(controlDir, "split"))
	if err != nil {
		log.Fatal("listen error:", err)
	}
	listeners = append(listeners, l)

	for {
		fd, err := l.Accept()
		if err != nil {
			log.Fatal("accept error:", err)
		}

		nextSplit()
		fd.Close()
	}
}

func main() {
	timerStart = time.Now()

	controlDir := flag.String("control", "", "control socket directory")
	flag.Parse()

	if *controlDir == "" {
		*controlDir = os.TempDir()
	}
	defer func() {
		for _, l := range listeners {
			l.Close()
		}
	}()

	app = cview.NewApplication()

	timerRunning = true

	inputConfig := cbind.NewConfiguration()

	inputConfig.Set("Space", func(ev *tcell.EventKey) *tcell.EventKey {
		nextSplit()
		return nil
	})

	mainTimer = cview.NewTextView()

	app.SetInputCapture(inputConfig.Capture)

	app.SetRoot(mainTimer, true)

	go listenControlFile(*controlDir)

	go handleUpdateTimer()

	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
