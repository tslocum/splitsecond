# splitsecond
[![CI status](https://gitlab.com/tslocum/splitsecond/badges/master/pipeline.svg)](https://gitlab.com/tslocum/splitsecond/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Speedrun timer

## Features

- Control via sockets

The main advantage over other Linux-compatible options is the control socket
interface.  Splits may be indicated by adding a global hotkey to your window
manager configuration which executes the following:

`socat - UNIX-CONNECT:/tmp/split`

## Download

```bash
go get gitlab.com/tslocum/splitsecond
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/splitsecond/issues).
