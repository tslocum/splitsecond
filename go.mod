module gitlab.com/tslocum/splitsecond

go 1.15

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gdamore/tcell/v2 v2.0.1-0.20201019142633-1057d5591ed1
	gitlab.com/tslocum/cbind v0.1.3
	gitlab.com/tslocum/cview v1.5.2-0.20201107170141-79a35fe4de6c
	golang.org/x/sys v0.0.0-20201107080550-4d91cf3a1aaf // indirect
)
